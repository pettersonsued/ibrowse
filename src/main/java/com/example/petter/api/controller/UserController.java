package com.example.petter.api.controller;

import com.example.petter.domain.model.UserEntity;
import com.example.petter.domain.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping(value = "users", produces = {"application/json"})
@Tag(name = "users")
public class UserController {


    private UserService userService;


    @GetMapping
    public List<UserEntity> listar(){
        return this.userService.listar();
    }

    @Operation(summary = "realiza a busca de um Usuario por Id", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{userId}")
    public ResponseEntity<UserEntity> buscar(@PathVariable Long userId){
        Optional<UserEntity> user = this.userService.getUser(userId);
        if(user.isPresent()){
            return ResponseEntity.ok(user.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza a adição de um novo Usuário", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Usuário cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserEntity adicionar(@Valid @RequestBody UserEntity user){
        String teste="";

        return this.userService.salvar(user);
    }

    @Operation(summary = "realiza a atualização de um Usuario", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{userId}")
    public ResponseEntity<UserEntity> atualizar(@PathVariable Long userId, @Valid @RequestBody UserEntity user){
        if(!this.userService.existeId(userId)){
            return ResponseEntity.notFound().build();
        }
        user.setId(userId);
        user = this.userService.salvar(user);
        return ResponseEntity.ok(user);
    }

    @Operation(summary = "realiza a exclusão de um Usuario", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> excluir(@PathVariable Long userId){
        if(!this.userService.existeId(userId)){
            return ResponseEntity.notFound().build();
        }
        this.userService.excluir(userId);
        return ResponseEntity.noContent().build();
    }
}
