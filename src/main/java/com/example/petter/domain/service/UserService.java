package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.UserEntity;
import com.example.petter.domain.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class UserService {


    private UserRepository userRepository;

    public List<UserEntity> listar(){
        return this.userRepository.findAll();
    }

    public UserEntity salvar(UserEntity user){
        boolean existeNome = this.userRepository.findByNome(user.getNome())
                .filter(u -> !u.equals(user)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.userRepository.save(user);
    }

    public Optional<UserEntity> getUser(Long userId){
        return this.userRepository.findById(userId);
    }

    public boolean existeId(Long userId){
        return this.userRepository.existsById(userId);
    }

    public void excluir(Long userId){
        this.userRepository.deleteById(userId);
    }

}
